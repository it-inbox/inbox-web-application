@extends('admin.template.layout')
@section('title','Dashboard')
@section('page-content')
    @breadcrumb(Dashboard:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-lg-3">
                <div class="list-group">
                    <a href="{{ route('user-list-view') }}">
                        <div class="list-group-item d-flex align-items-center">
                            <div class="width-40 height-40 d-flex align-items-center justify-content-center bg-primary-lighter text-white rounded ml-n1">
                                <i class="fa fa-users fa-lg text-primary"></i>
                            </div>
                            <div class="flex-fill pl-3 pr-3">
                                <div class="font-medium-3">{{ number_format($userCount) }}</div>
                                <div class="font-small-2">Total Users</div>
                            </div>
                        </div>
                    </a>
                    <a href="" class="d-none">
                        <div class="list-group-item d-flex align-items-center">
                            <div
                                    class="width-40 height-40 d-flex align-items-center justify-content-center bg-success-lighter text-white rounded ml-n1">
                                <i class="fa fa-user-plus fa-lg text-success"></i>
                            </div>
                            <div class="flex-fill pl-3 pr-3">
                                <div class="font-medium-3">0</div>
                                <div class="font-small-2">Active Users</div>
                            </div>
                        </div>
                    </a>
                    {{--<a href="{{ route('admin-user-view', ['user_status' => 'inactive']) }}">--}}
                        {{--<div class="list-group-item d-flex align-items-center">--}}
                            {{--<div--}}
                                    {{--class="width-40 height-40 d-flex align-items-center justify-content-center bg-danger-lighter text-white rounded ml-n1">--}}
                                {{--<i class="fa fa-user-times fa-lg text-danger"></i>--}}
                            {{--</div>--}}
                            {{--<div class="flex-fill pl-3 pr-3">--}}
                                {{--<div class="font-medium-3">{{ number_format($user_count->inactive) }}</div>--}}
                                {{--<div class="font-small-2">Inactive Users</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                    {{--<a href="{{ route('admin-user-view', ['dateRange' => \Carbon\Carbon::now()->format('d M Y') . ' - ' . \Carbon\Carbon::now()->format('d M Y')]) }}">--}}
                        {{--<div class="list-group-item d-flex align-items-center">--}}
                            {{--<div--}}
                                    {{--class="width-40 height-40 d-flex align-items-center justify-content-center bg-warning-light text-white rounded ml-n1">--}}
                                {{--<i class="fa fa-user-circle fa-lg text-dark"></i>--}}
                            {{--</div>--}}
                            {{--<div class="flex-fill pl-3 pr-3">--}}
                                {{--<div class="font-medium-3">{{ number_format($user_count->today) }}</div>--}}
                                {{--<div class="font-small-2">Today's Users</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card no-border widget-loader-bar m-b-10">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="card-header top-left top-right">
                                            <div class="card-title">
                                                <span class="fs-11 all-caps">Contact Inquiries <i class="fa fa-chevron-right"></i></span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           class="card-refresh"
                                                           data-original-title="Uploaded Documents by User which are still pending to Verify">
                                                            <i class="card-icon card-icon-refresh"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                                            <h3 class="no-margin p-b-5"> 0</h3>
                                            <span class="small hint-text pull-left">Contact Inquiries</span>
                                            <a href="javascript:void(0);"
                                               class="pull-right small text-primary">View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection