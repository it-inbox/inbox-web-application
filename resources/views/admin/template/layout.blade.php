<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>{{ env('APP_NAME')  }} - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="{{ asset('admin-assets/ico/60.png')  }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('admin-assets/ico/76.png')  }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('admin-assets/ico/120.png')  }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('admin-assets/ico/152.png')  }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('admin-assets/company/favicon.png')  }}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="Meet pages - The simplest and fastest way to build web UI for your dashboard or app." name="description" />
    <meta content="Ace" name="author" />
    <link href="{{ asset('admin-assets/plugins/pace/pace-theme-flash.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin-assets/plugins/bootstrap/css/bootstrap.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin-assets/plugins/font-awesome/css/font-awesome.css')   }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin-assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('admin-assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('admin-assets/plugins/nvd3/nv.d3.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('admin-assets/plugins/rickshaw/rickshaw.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin-assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{  asset('admin-assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{ asset('admin-assets/plugins/mapplic/css/mapplic.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin-assets/css/dashboard.widgets.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/dropzone/css/dropzone.css')  }}">
    <link class="main-stylesheet" href="{{  asset('admin-assets/css/themes/light.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('admin-assets/css/custom.css')  }}">
    @yield('import-css')
    @yield('page-css')
</head>
<body class="fixed-header dashboard menu-pin">
@include('admin.template.sidebar')
<div class="page-container ">
    @include('admin.template.header')
    <div class="page-content-wrapper ">
        <div class="content">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error')  }}
                </div>
            @elseif(session('success'))
                <div class="alert alert-success">
                    {{  session('success')  }}
                </div>
            @endif
            @yield('page-content')
        </div>
        @include('admin.template.footer')
    </div>
</div>
{{--<div class="overlay hide" data-pages="search">--}}
{{--<div class="overlay-content has-results m-t-20">--}}
{{--<div class="container-fluid">--}}
{{--<img class="overlay-brand" src="{{ asset('admin-assets/img/logo.png')  }}" alt="logo" data-src="{{ asset('admin-assets/img/logo.png')  }}" width="100">--}}
{{--<a href="#" class="close-icon-light overlay-close text-black fs-16">--}}
{{--<i class="pg-close"></i>--}}
{{--</a>--}}
{{--</div>--}}
{{--<div class="container-fluid">--}}
{{--<input id="overlay-search" class="no-border overlay-search bg-transparent" placeholder="Search..." autocomplete="off" spellcheck="false">--}}
{{--<br>--}}
{{--<div class="inline-block m-l-10">--}}
{{--<p class="fs-13">Press enter to search</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--</div>--}}
<script src="{{  asset('admin-assets/plugins/feather-icons/feather.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/liga.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/jquery/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/popper/umd/popper.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
<script src="{{ asset('admin-assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{  asset('admin-assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{  asset('admin-assets/plugins/classie/classie.js') }}"></script>
<script src="{{  asset('admin-assets/plugins/nvd3/lib/d3.v3.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/nvd3/nv.d3.min.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/nvd3/src/utils.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/nvd3/src/tooltip.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/nvd3/src/interactiveLayer.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/nvd3/src/models/axis.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/nvd3/src/models/line.js') }}" type="text/javascript"></script>
<script src="{{  asset('admin-assets/plugins/nvd3/src/models/lineWithFocusChart.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/rickshaw/rickshaw.min.js')  }}"></script>
<script src="{{ asset('admin-assets/plugins/dropzone/dropzone.min.js')  }}"></script>
<script src="{{ asset('admin-assets/plugins/quill/quill.min.js')  }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/plugins/moment/moment.min.js')  }}"></script>
<script src="{{ asset('admin-assets/plugins/bootstrap-daterangepicker/daterangepicker.js')  }}"></script>
<script src="{{ asset('admin-assets/js/form_elements.js')  }}"></script>
<script src="{{  asset('admin-assets/js/dashboard.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin-assets/js/pages.js')  }}"></script>
<script src="{{ asset('admin-assets/js/scripts.js?v=oct').date('Y') }}" type="text/javascript"></script>
@yield('import-js')
@yield('page-js')
<script>
    $(document).ready(function(){
        setTimeout(function(){
            $('.alert-success').hide();
        },2500);
    });
</script>
</body>
</html>