@php
    $request = request()->segments();
@endphp
<nav class="page-sidebar" data-pages="sidebar">
    <div class="sidebar-header text-center">
        <img src="{{ asset('admin-assets/company/logo.png') }}" alt="logo" class="brand" data-src="{{ asset('admin-assets/company/logo.png') }}" data-src-retina="{{ asset('assets/img/logo_2x.png')  }}" width="100" height="50">
    </div>
    <div class="sidebar-menu">
        <ul class="menu-items">
            <li class="m-t-10 {{ end($request) == 'dashboard' ? 'active': ''  }}">
                <a href="{{ route('admin-dashboard')  }}" class="detailed">
                    <span class="title">Dashboard</span>
                </a>
                <span class="icon-thumbnail"><i data-feather="home"></i></span>
            </li>
            <li class="{{ end($request) == 'user' ? 'active': ''  }}">
                <a href="{{ route('user-list-view')  }}"><span class="title">Users</span></a>
                <span class="icon-thumbnail"><i data-feather="user"></i></span>
            </li>
            <li class="{{ end($request) == 'settings' ? 'active': ''  }}">
                <a href="{{ route('admin-settings')  }}"><span class="title">Settings</span></a>
                <span class="icon-thumbnail"><i data-feather="settings"></i></span>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
</nav>