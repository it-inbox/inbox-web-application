@extends('admin.template.layout')
@section('title','User')
@section('page-content')
   @breadcrumb(Dashboard:admin-dashboard,User:active)
   <div class="container-fluid container-fixed-lg">
      <div class="card d-none">
         <div class="card-body">
            <form action="" method="get">
               <div class="row">
                  <div class="col-md-9">
                     <div class="row">
                        <div class="col-md-4">
                           <div class="form-group input-group transparent">
                              <div class="input-group-prepend">
                                 <span class="input-group-text transparent"><i class="pg-icon">search</i></span>
                              </div>
                              <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                           </div>
                        </div>
                        <div class="col-md-1 mt-1">
                           <button class="btn btn-danger"> Search</button>
                        </div>
                        {{--<div class="col-md-4">--}}
                        {{--<div class="input-group">--}}
                        {{--<div class="input-group-prepend"><span class="input-group-text"><i--}}
                        {{--class="fa fa-calendar"></i></span></div>--}}
                        {{--<input type="text" name="dateRange" class="form-control date-range"--}}
                        {{--value="{{ Request::get('dateRange') }}"--}}
                        {{--placeholder="Date range for Filter" readonly>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive m-t-10">
               <table class="table table-hover table-bordered">
                  <thead>
                  <tr>
                     <th>Actions</th>
                     <th>User Name</th>
                     <th>Email</th>
                     <th>Contact Number</th>
                     <th>Gender</th>
                     <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($users as $index => $user)
                     <tr>
                        <td>
                           <a href="{{ route('admin-user-account-access',$user->id)  }}" target="_blank" class="btn btn-primary btn" title="View Account"><i class="fa fa-universal-access"></i></a>
                           <a href="{{ route('edit_user',$user->id)  }}" class="btn btn-warning btn" title="Edit User"><i class="fa fa-pencil"></i></a>
                           <a href="#" class="btn btn-  open-modal" data-toggle="modal" data-target="#exampleModal_{{ $user->id }}" data-user-id="{{ $user->id }}">
                              <i class="fa fa-eye"></i></a>
                           <!-- Modal -->
                           <div class="modal fade" id="exampleModal_{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document" style="max-width: 80%;">
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <!-- Add tabs here -->
                                       <ul class="nav nav-tabs" id="profileTabs">
                                          <li class="nav-item">
                                             <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile">Profile Info</a>
                                          </li>
                                          <li class="nav-item">
                                             <a class="nav-link" id="company-tab" data-toggle="tab" href="#company">Company Info</a>
                                          </li>
                                          <li class="nav-item">
                                             <a class="nav-link" id="communication-tab" data-toggle="tab" href="#communication">Communication Preference</a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="modal-body">
                                       <div class="tab-content">
                                          <div class="tab-pane fade show active" id="profile">
                                             <h6 style="display: inline-block; margin-right: 5px;">Full Name</h6><span>{{ $user->display_name() }}</span><br>
                                             <h6 style="display: inline-block; margin-right: 5px;">Email</h6><span>{{ $user->email }}</span><br>
                                             <h6 style="display: inline-block; margin-right: 5px;">Contact Number</h6><span>{{ $user->contact_number }}</span><br>
                                             <h6 style="display: inline-block; margin-right: 5px;">Address</h6><span>{{ $user->address }}</span>

                                          </div>
                                          <div class="tab-pane fade" id="company">
                                             <h6 style="display: inline-block; margin-right: 5px;">Company Name</h6><span>{{ $user->company_name }}</span><br>
                                             <h6 style="display: inline-block; margin-right: 5px;">Company Email</h6><span>{{ $user->company_email }}</span><br>
                                             <h6 style="display: inline-block; margin-right: 5px;">Contact Number</h6><span>{{ $user->contact_number }}</span><br>
                                             <h6 style="display: inline-block; margin-right: 5px;">Company Address</h6><span>{{ $user->address }}</span><br>


                                          </div>
                                          <div class="tab-pane fade" id="communication">
                                             <h6 style="display: inline-block; margin-right: 5px;">Prefered Language</h6><span>{{ $user->lang }}</span><br>
                                             <h6 style="display: inline-block; margin-right: 5px;">Communication Channel Preferences</h6><span>{{ $user->channel_type }}</span><br>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- Modal -->
                        </td>
                        <td>{{ $user->display_name() }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->mobile }}</td>
                        <td>
                           @if($user->gender != '')
                              @if($user->gender == 1 || $user->gender == 'male' || $user->gender == 'Male')
                                 <span class="badge badge-success light border-0">Male</span>
                              @else
                                 <span class="badge badge-danger light border-0">Female</span>
                              @endif
                           @else
                              <span>N.A</span>
                           @endif
                        </td>
                        <td>
                           @if($user->status == 1)
                              <span class="badge badge-success light border-0">Active</span>
                           @else
                              <span class="badge badge-danger light border-0">InActive</span>
                           @endif
                        </td>
                     </tr>
                  @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="row mb-5">
         <div class="col-md-8">
            {{--<span>Showing {{ $users->perPage()  }} Of {{ $users->total()  }} Results</span>--}}
         </div>
         <div class="col-md-4 text-right">
            @if ($users->lastPage() > 1)
               <ul class="pagination">
                  <li class="{{ ($users->currentPage() == 1) ? ' disabled' : '' }}">
                     <a href="{{ $users->url(1) }}" class="btn btn-default">Previous</a>
                  </li>
                  @for ($i = 1; $i <= $users->lastPage(); $i++)
                     <li class="{{ ($users->currentPage() == $i) ? ' active' : '' }}">
                        <a href="{{ $users->url($i) }}" class="circle">{{ $i }}</a>
                     </li>
                  @endfor
                  <li class="{{ ($users->currentPage() == $users->lastPage()) ? ' disabled' : '' }}">
                     <a href="{{ $users->url($users->currentPage()+1) }}" class="btn btn-default">Next</a>
                  </li>
               </ul>
            @endif
         </div>
      </div>
   </div>
@stop
@section('page-css')
   <style>
      .pagination{
         float: right;
      }
   </style>
@endsection
@section('page-js')
   <script>
       $('.date-range').daterangepicker({
           locale: {
               format: 'DD MMM YYYY'
           },
           autoUpdateInput: false,
           buttonClasses: ['btn', 'btn-sm'],
           applyClass: 'btn-success',
           cancelClass: 'btn-danger',
           ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
       }).on('apply.daterangepicker', function(ev, picker) {
           $(this).val(picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY'));
       }).on('cancel.daterangepicker', function(ev, picker) {
           $(this).val('');
       });

   </script>
@endsection