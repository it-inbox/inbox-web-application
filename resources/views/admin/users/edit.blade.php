@extends('admin.template.layout')
@section('title','Edit User')
@section('page-content')
    @breadcrumb(Home:admin-dashboard,User:user-list-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body p-4 m-0">
                        <form action="{{ route('user-update')  }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{ $user->id  }}" name="user_id">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="form-group form-group-default">
                                        <label>Full Name</label>
                                        <input type="text" class="form-control" name="full_name" value="{{ $user->full_name  }}">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" value="{{ $user->email  }}">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Contact Number</label>
                                        <input type="number" class="form-control" name="phone" value="{{ $user->mobile  }}">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Password</label>
                                        <input type="password" class="form-control passwordInput" id="dz-password" name="password">
                                        <div class="input-group-append position-relative pull-right">
                                            <button class="btn btn-primary float-right" type="button" onclick="showPassword(this)">
                                                <i class="fa fa-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Address</label>
                                        <textarea name="address" rows="10" class="form-control">{{ $user->address  }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" name="status" id="radioInline1" value="1" {{ $user->status == \App\Models\User::ACTIVE ? 'checked' : ''  }}>
                                            <label for="radioInline1">
                                                Active
                                            </label>
                                        </div> <div class="form-check form-check-inline complete">
                                            <input type="radio" name="status" id="radioDisabled2" value="2" {{ $user->status == \App\Models\User::INACTIVE ? 'checked' : ''  }}>
                                            <label for="radioDisabled2">
                                                In-active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="profile-photo">
                                        <label>Profile Picture</label>
                                        <div class="dz-default dlab-message upload-img mb-3">
                                            <div class="fallback">
                                                <img class="img-circle profile_img" src="" alt="" />
                                                <input name="profile_photo" type="file" id="profile_photo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="current_photo text-center">
                                        <label>Current Profile</label>
                                        <img src="{{ asset($user->profile_photo)  }}" class="img-circle" alt="">
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-lg-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Contact Number</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Created / Updated</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($histories as $key => $history)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $history->properties['attributes']['full_name'] }}</td>
                                    <td>{{ $history->properties['attributes']['email'] }}</td>
                                    <td>{{ $history->properties['attributes']['mobile'] }}</td>
                                    <td>{{ $history->properties['attributes']['address'] }}</td>

                                    <td>
                                        @if($history->properties['attributes']['status'] == \App\Models\User::ACTIVE)
                                            <span class="badge badge-success">Active</span>
                                        @else
                                            <span class="badge badge-danger">In-active</span>
                                        @endif
                                    </td>
                                    <td>{{  $history->created_at  }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-js')
    <script>
        $(document).ready(function(){
            $('#profile_photo').change(function (e) {
                e.preventDefault();
                const file = this.files[0];
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        $('.profile_img').attr('src', event.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            })
        });
        function showPassword(element){
            var passwordElement = $(element).parent().parent().find('input.passwordInput');

            var inputType = passwordElement.attr('type');

            if (inputType === 'password') {
                $(element).html('<i class="fa fa-eye"></i>');
                passwordElement.attr('type', 'text');
            }
            else {
                $(element).html('<i class="fa fa-eye-slash"></i>');
                passwordElement.attr('type', 'password');
            }
        }
    </script>
@endsection