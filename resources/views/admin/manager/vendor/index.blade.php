@extends('admin.template.layout')
@section('title','Manage Vendors')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Vendors:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ route('admin-manage-vendors.create')  }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Vendor Name</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vendors as $index => $vendor)
                        <tr id="accordion">
                            <td>
                                <a href="{{  route('admin-manage-vendors.edit',[$vendor->id]) }}"
                                   class="btn btn-warning btn" data-toggle="tooltip" title="Edit"><i
                                            class="fa fa-pencil text-dark"></i></a>
                                <a data-bs-toggle="collapse" data-bs-parent="#accordion" href="#vendorDetails{{ $index + 1  }}" class="btn btn- mr-5" title="View Details">
                                    <i class="fa fa-eye text-dark"></i>
                                </a>
                            </td>
                            <td>{{ $vendor->name  }}</td>
                            <td>
                                @if($vendor->status == \App\Models\Vendor::ENABLE)
                                    <span class="badge badge-success">Enable</span>
                                @else
                                    <span class="badge badge-danger">disable</span>
                                @endif
                            </td>
                        </tr>
                        <tr class="vendor-row-details">
                            <td colspan="10" class="p-0">
                                <div id="vendorDetails{{ $index + 1 }}" class="accordion-body collapse">
                                    <div class="p-2 table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>URL</th>
                                                <th>Secret Key</th>
                                                <th>client Key</th>
                                                <th>username</th>
                                                <th>password</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{  $vendor->url  }}</td>
                                                <td>{{  $vendor->secret_key ? $vendor->secret_key : 'N.A'  }}</td>
                                                <td>{{  $vendor->client_key ? $vendor->client_key : 'N.A' }}</td>
                                                <td>{{  $vendor->username ? $vendor->username : 'N.A'  }}</td>
                                                <td>{{  $vendor->password ? $vendor->password : 'N.A'  }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection