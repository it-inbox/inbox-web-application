<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('profile_photo')->nullable();
            $table->integer('role_id');
            $table->string('full_name');
            $table->string('username')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email');
            $table->string('password');
            $table->string('mobile')->nullable();
            $table->integer('country')->nullable();
            $table->tinyInteger('gender')->comment('1:Male, 2:FeMale,3:Transgender')->default(0)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->longText('address')->nullable();
            $table->tinyInteger('status')->comment('1:Active, 2: Inactive')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
