<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\Admin\MasterController;

Route::prefix('admin')->group(function () {
    Route::get('/',function(){
        return redirect()->route('admin-login');
    });
    Route::get('login',[LoginController::class,'index'])->name('admin-login');
    Route::post('auth',[LoginController::class,'Auth'])->name('admin-auth');

    Route::group(['middleware' => 'admin.auth'],function(){
        Route::get('dashboard',[DashboardController::class,'index'])->name('admin-dashboard');

        Route::group(['prefix' => 'manager'], function () {
            Route::get('profile', [LoginController::class,'profile'])->name('admin-manager-profile');
            Route::post('profile', [LoginController::class,'profile'])->name('admin-manager-profile');
        });

        Route::group(['prefix' => 'user'],function (){
            Route::get('/',[UserController::class,'index'])->name('user-list-view');
            Route::get('create',[UserController::class,'create'])->name('user-list-create');
            Route::post('store',[UserController::class,'store'])->name('user-list-store');
            Route::get('edit_user/{id}',[UserController::class,'edit'])->name('edit_user');
            Route::post('user-update',[UserController::class,'update'])->name('user-update');
            Route::get('delete_user/{id}',[UserController::class,'deleteUser'])->name('delete_user');

            Route::get('admin-user-account-access/{id}',[LoginController::class,'adminUserAccountAccess'])->name('admin-user-account-access');
        });

        Route::group(['prefix' => 'settings'],function(){
            Route::get('/',[SettingController::class,'index'])->name('admin-settings');
            Route::post('update',[SettingController::class,'update'])->name('admin-settings-update');
        });

    
        Route::group(['prefix' =>'currencies'],function(){
            Route::get('/',[MasterController::class,'currencyIndex'])->name('currencies_list_view');
            Route::get('create',[MasterController::class,'currencyCreate'])->name('currencies_create');
            Route::post('create',[MasterController::class,'currencyCreate'])->name('currencies_create');
            Route::get('update/{id}',[MasterController::class,'currencyUpdate'])->name('currencies_update');
            Route::post('update/{id}',[MasterController::class,'currencyUpdate'])->name('currencies_update');
        });
        Route::group(['prefix' =>'languages'],function(){
            Route::get('/',[MasterController::class,'languageIndex'])->name('languages_list_view');
            Route::get('create',[MasterController::class,'languageCreate'])->name('languages_create');
            Route::post('create',[MasterController::class,'languageCreate'])->name('languages_create');
            Route::get('update/{id}',[MasterController::class,'languageUpdate'])->name('languages_update');
            Route::post('update/{id}',[MasterController::class,'languageUpdate'])->name('languages_update');
        });
    });
    Route::get('logout',[LoginController::class,'logout'])->name('admin-logout');
});

 