<?php

namespace App\Http\Controllers;

use App\Models\ContactInquiry;
use App\Models\Country;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){

        $userCount = User::count();

        return view('admin.dashboard',[
            'userCount' => $userCount
        ]);
    }

}
