<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactInquiry;
use Illuminate\Http\Request;
//mail
use Illuminate\Support\Facades\Mail;

class ContactInquiryController extends Controller
{
    public function index()
    {
        $contactInquiries = ContactInquiry::orderBy('id','desc')->paginate(10);

        return view('admin.contact_inquiry.index',[
            'contactInquiries' =>  $contactInquiries
        ]);
    }

    public function reply($id)
    {
        $contactInquiry = ContactInquiry::find($id);

        return view('admin.contact_inquiry.reply', [
            'contactInquiry' => $contactInquiry
        ]);
    } 


    public function storeReply(Request $request)
    {
        if($request->flag == 'reply'){
            $validatedData = $request->validate([
                'replay_message' => 'required|string',
            ]);
            $contactInquiry = ContactInquiry::find($request->id);
            $contactInquiry->replay_message = $request->input('replay_message');
            $contactInquiry->save();
    
            //mail code
            if(env('APP_ENV') != 'local'){
                Mail::send('mail.contact_inquiry', [
                    'contactInquiry' => $contactInquiry
                ], function ($mail) use ($contactInquiry) {
                    $mail->from(env('MAIL_USERNAME'), config('app.name'));
                    $mail->to($contactInquiry->email)->subject('Contact Inquiry');
                });
            }
        }
        else{
            $validatedData = $request->validate([
                'reason' => 'required|string',
            ]);
            $contactInquiry = ContactInquiry::find($request->id);
            $contactInquiry->reason = $request->input('reason');
            $contactInquiry->status = $request->input('status');
            $contactInquiry->save();

            if(env('APP_ENV') != 'local'){
                Mail::send('mail.contact_inquiry', [
                    'contactInquiry' => $contactInquiry
                ], function ($mail) use ($contactInquiry) {
                    $mail->from(env('MAIL_USERNAME'), config('app.name'));
                    $mail->to($contactInquiry->email)->subject('Contact Inquiry');
                });
            }
        }

        return redirect()->route('contact-inquiries.view')->with(['success' => 'Reply submitted successfully!']);
    }



}
