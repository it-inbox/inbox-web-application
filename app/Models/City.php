<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'country_id','state_id','state_code','name'
    ];
}
